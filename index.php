<?php 
require_once 'model/Person.php';
$p = new Person();
$p->populate(['id' => null, 'name' => 'foo', 'email' => 'bar', 'password' => 'baz'])->create();
var_dump($p::find());
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8" />
<title>php-crud</title>

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

</head>
<body>
	<div class="container">
	   <?php include_once 'partial/form.phtml' ?>
		<div class="row">
			<div class="span12">
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nome</th>
							<th>Ações</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>