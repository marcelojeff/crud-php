<?php
require_once 'model/Database.php';
$pdo = Database::getConnection();
$schema = <<<DB
  CREATE TABLE IF NOT EXISTS People (
    "id" INTEGER NOT NULL AUTO-INCREMENT,
    "name" TEXT(256) NOT NULL,
    "email" TEXT(256) NOT NULL,
    "password" TEXT(32) NOT NULL,
    PRIMARY KEY ("id")
  );
DB;
if($pdo->exec($schema)){
  echo "Base de dados criada.";
} else {
  echo "Falha ao criar BD\n";
  var_dump( $pdo->errorInfo() );
}
