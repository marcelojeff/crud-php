<?php
abstract class Database {

	public static $pdo;

	/**
	 * As this is just a test, We are using a memory database
	 * @return PDO
	 */
	public static function getConnection(){
		$dsn = 'sqlite:/tmp/db.sq3';
		if(!isset(self::$pdo)){
			self::$pdo = new PDO ( $dsn, null, null, [
				 PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
			);
		}
		return self::$pdo;
	}
	public static function getHandler($query, $params){
		$connection = self::getConnection();
		$handler = $connection->prepare($query);
		if(!$handler->execute($params)){
			throw new Exception('An error has occured while query execution: ' . $handler->errorInfo()[2]);
		}
		return $connection;
	}
	public static function exec($query, $params){
		return self::getHandler($query, $params);
	}
	public static function findAll($query, $params = null){
		return self::getHandler($query, $params)->fetchAll();
	}
	public static function findOne($query, $params){
		return self::getHandler($query, $params)->fetch();
	}
}
