<?php
require_once 'model/Database.php';
class Person
{
    private $id;
    private $name;
    private $email;
    private $password;
    private $isPopulated = false;
    
    public function getId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getPassword(){
        return $this->password;
    }
    public function setId($value){
        $this->id = $value;
        return $this;    
    }
    public function setName($value){
        $this->name = $value;
        return $this;
    }
    public function setEmail($value){
        $this->email = $value;
        return $this;
    }
    public function setPassword($value){
        $this->password = md5($value);
    }
    public static function find($id = null) {
        $query = 'SELECT * FROM Person';
        if($id){
            $query .= ' WHERE id = :id';
            $params = [':id' => $id];
            $data = Database::findOne($query, $params);
            $entity = new self();
            return $entity->populate($data);
        }
        $data = Database::findAll($query);
        $list = [];
        foreach ($data as $row){
            $entity = new self();
            $list [] = $entity->populate($row);
        }
        return $list;
    }
    public function create(){
        $query = 'INSERT into Person (name, email, password) VALUES (":name", ":email", ":password")';
        $params = $this->getParams(true);
        return Database::exec($query, $params);
    }
    public function edit(){
        $query = 'UPDATE Person SET id = :id, name = ":name", email = ":email", password = ":password"';
        $params = $this->getParams();
        return Database::exec($query, $params);
    }
    public function delete(){
        $query = 'DELETE from Person WHERE id = :id';
        $params = $this->getParams();
        return Database::exec($query, $params);
    }
    public function populate($data){
        $this->setId($data['id']);
        $this->setName($data['name']);
        $this->setEmail($data['email']);
        $this->setPassword($data['password']);
        $this->isPopulated = true;
        return $this;
    }
    private function getParams($ignoreId = false){
        if(!$this->isPopulated){
            throw new RuntimeException('Object must have all values seted');
        }
        $params = [
            ':name' => $this->getName(),
            ':email' => $this->getEmail(),
            ':password' => $this->getPassword()
        ];
        if(!$ignoreId){
            $params[':id'] = $this->getId();
        }
        return $params;
    }
}