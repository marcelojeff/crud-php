<?php
class FlashMessenger {
	public static function getSuccessMessage(){
		$html = '';
		$adwordsScript = '';
		if($adwords = Database::getPageContent('adwords')){
		    $adwordsScript = $adwords;
		}
		if(isset($_SESSION['flash-messages']['success'])){
			$html = sprintf('%s<div class="flash-message flash-message-success">
				%s
			</div>', $adwordsScript, $_SESSION['flash-messages']['success']);
			unset($_SESSION['flash-messages']['success']);
		}
		return $html;
	}
	public static function setSuccessMessage($message){
		$_SESSION['flash-messages']['success'] = $message;
	}
	public static function setErrorMessage($message){
		$_SESSION['flash-messages']['error'] = $message;
	}
	public static function getErrorMessage(){
		$html = '';
		if(isset($_SESSION['flash-messages']['error'])){
			$html = sprintf('<div class="flash-message flash-message-error">
				%s
			</div>', $_SESSION['flash-messages']['error']);
			unset($_SESSION['flash-messages']['error']);
		}
		return $html;
	}
}